require "minitest/autorun"

require_relative 'resistor_builder'

class TestResistorBuilder < Minitest::Test
  # # I want a resistor of 100 ohms, and have 15 ohm resistors.
  # # There is no solution to this, given the default parameters.
  # # The `solution` method should return and empty array.
  # def test_no_solution_one
  #   solutions = ResistorBuilder.new(100, [15]).solutions

  #   assert_equal 0, solutions.length
  # end

  # # I want a resistor of 200 ohms, and have 100 ohm resistors.
  # # The solution should be two 100-ohm resistors in series.
  # def test_simple_series_two
  #   solutions = ResistorBuilder.new(200, [100]).solutions

  #   assert_equal 1, solutions.length

  #   assert_equal :series, solutions.first[0]
  #   assert_equal [100, 100], solutions.first[1]
  # end

  # # I want a resistor of 150 ohms, and have 50 ohm resistors.
  # # The solution should be three 50-ohm resistors in series.
  # def test_simple_series_three
  #   solutions = ResistorBuilder.new(150, [50]).solutions

  #   assert_equal 1, solutions.length

  #   assert_equal :series, solutions.first[0]
  #   assert_equal [50, 50, 50], solutions.first[1]
  # end

  # # I want a resistor of 150 ohms, and have 50 ohm resistors.
  # # However, I only have two of the, so there can be no solution.
  # # The `solution` method should return and empty array.
  # def test_no_solution_two
  #   solutions = ResistorBuilder.new(150, [50], maximum: 2).solutions

  #   assert_equal 0, solutions.length
  # end

  # I want a resistor of 150 ohms, and I have 90 and 60 ohm resistors.
  # The solution should be a 60 ohm and 90 ohm resistor in series
  def test_unequal_sizes_one
    solutions = ResistorBuilder.new(150, [90, 60]).solutions

    assert_equal 1, solutions.length

    assert_equal :series, solutions.first[0]
    assert_equal [60, 90], solutions.first[1]
  end

  # # I want a resistor of 50 ohms, and have 100 ohm resistors.
  # # The solution should be two 100-ohm resistors in parallel.
  # def test_simple_parallel_two
  #   solutions = ResistorBuilder.new(50, [100]).solutions

  #   assert_equal 1, solutions.length

  #   assert_equal :parallel, solutions.first[0]
  #   assert_equal [100, 100], solutions.first[1]
  # end

  # # I want a resistor of 156.80 ohms, and have 330, 470, and 820 ohm resistors.
  # # The solution should be one each of 330, 470, and 820 resistors in parallel.
  # def test_simple_parallel_three
  #   solutions = ResistorBuilder.new(156.80, [330, 470, 820]).solutions
  #   assert_equal 1, solutions.length

  #   assert_equal :parallel, solutions.first[0]
  #   assert_equal [330, 470, 820], solutions.first[1]
  # end

  # # I want a resistor of 156.0 ohms, and have 470 ohm resistors.
  # # The solution will return a value within 1% of the required value.
  # # The solution should be three 470-ohm resistors in parallel.
  # def test_simple_parallel_three_approximate
  #   solutions = ResistorBuilder.new(156, [470], error_margin: 1.0).solutions

  #   assert_equal 1, solutions.length

  #   assert_equal :parallel, solutions.first[0]
  #   assert_equal [470, 470, 470], solutions.first[1]
  # end

  # # I want a resistor of 156.0 ohms, and have 330, 470, and 820 ohm resistors.
  # # The solution will return values within 2% of the required value.
  # # There will be two solutions returned:
  # # * [:parallel, [330, 470, 820]]
  # # * [:parallel, [470, 470, 470]]
  # def test_simple_parallel_three_approximate_multiple
  #   solutions = ResistorBuilder.new(156, [330, 470, 820], error_margin: 2.0).solutions

  #   assert_equal 2, solutions.length

  #   assert solutions.include?([:parallel, [330, 470, 820]])
  #   assert solutions.include?([:parallel, [470, 470, 470]])
  # end

  # # I want a resistor of 100.0 ohms, and have 100 and 200 ohm resistors.
  # # The solution will return multiple solutions, both parallel and serial.
  # # ThHe returned solutions must include:
  # # * [:series, [100]]
  # # * [:parallel, [100]]
  # # * [:parallel, [200, 200]]
  # def test_serial_and_parallel_returns_one
  #   solutions = ResistorBuilder.new(100, [100, 200], error_margin: 0.0).solutions

  #   assert_equal 3, solutions.length


  #   assert solutions.include?([:series, [100]])
  #   assert solutions.include?([:parallel, [100]])
  #   assert solutions.include?([:parallel, [200, 200]])
  # end
end

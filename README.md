# Resistor Builder Exercise

An exercise to write an program that can use resistors in series and parallel
to give a given value.

It is *not* required to understand what a resistor is or how they work, only
to understand the formula for resistors in series and parallel. A minimal explanation is provided here, a more complete explanation can [be found on Wikipedia](https://en.wikipedia.org/wiki/Resistor#Series_and_parallel_resistors).

## Installation

Clone the repository with `git clone https://gitlab.com/xunker/resistor-builder-exercise.git`

Change directory: `cd resistor-builder-exercise`

Install dependencies: `bundle install`

To run tests: `rake`

## Background

Sometimes, when working on electronics, you will need a resistor of a particular
_value_ ("value" here means "the resistance in Ohms"), but you don't have it.
That could be because it's a value that is uncommon or you have simply run out
of resistors of that value.

But the world of electronics lets you use the resistors you have to build a
new resistor with the value you need. You can do this by taking resistors of
smaller values and putting them in a line with each other ("in series"), or by
putting then next to each in a row ("in parallel").

Putting several resistors "in series" makes a larger resistor that has the value
of all the smaller resistors **added together**. See "Formulas => Series" for
more information.

Puting several resistors "in parallel" makes a resistor that has a value
**lower than** the resistors added together. See "Formulas => Parallel" for
more information.

## The Challenge

Write a program that call tell you what resistors you need to use in series
or parallel in order to get a resistor of the *value* you really want.

### Criteria

The program will need to accept and argument that is the  the value of the
resistor of you *want*, as well as a list of the resistors you *have available
to use*.

The program will return the resistors to be used and in what configuration
(*series* or *parallel*).

The program will also accept two additional *optional* arguments:
* Maximum number of resistors (in series or parallel) that can be used the solution
* Margin of error allowed (as a percent). For example, a margin of error of 1%
would mean a solution that gives you 99 ohms would be acceptable if you asked
for 100 ohms.

It is assumed that you have an infinite amount of resistors of each value on
hand, you do not need to worry about the *number of each value* used in your
solution, only the *total used*.

It is NOT required to combine *series* and *parallel* solutions in to one. The
returned solutions need to be only for *series* or *parallel* and not a
combination of both together.

### How to write your solution

The file you will be working on is `resistor_builder.rb`. There are two methods
you will implement: `#initialize` and `#solutions`.

`#initialize` sets up the parameters for the resistor you wish to build.

`#solutions` returns the possible solutions as an array.

There are tests written in `resistor_builder_test.rb`, you can run them by
running:

```
$ rake
```

Each test in the test file has a comment that explains the test as well as the
expected result. The object of this excercise is to make all of these pass by
adding the missing code to `resistor_builder.rb`.

The tests get harder as they go, so do not be discouraged that the tests
at the bottom of the file are failing: fix this easiest tests first and then
move to the next.

## Formulas

### Series

When two resistors are in series (one after another), their total resistance is
the sum of the individual resistances. Example:

```
   +---------+   +---------+
   |         |   |         |
---+  100 Ω  +---+  100 Ω  +--- = 200 Ω
   |         |   |         |
   +---------+   +---------+

     100 ohms  +   100 ohms     = 200 ohms
```

The formula can be expressed in mathematics as:

```math
R_\mathrm{eq} = R_1  + R_2 + \cdots + R_n.
```

...but can be better understood as Ruby code:

```ruby
resistor_one   = 100.0
resistor_two   = 100.0

resistor_one + resistor_two
# => 200
```


### Parallel

The total resistance of resistors connected in parallel is the reciprocal of
the sum of the reciprocals of the individual resistors. Example with equal
vaules of resistors:

```
--------+-------------+
        |             |
   +----+----+   +----+----+
   |         |   |         |
   |  100 Ω  |   |  100 Ω  |
   |         |   |         |
   +----+----+   +----+----+
        |             |
        +-------------+-------- = 50 Ω

1 / ( (1 / 100 ohms) + (1 / 100 ohms) ) = 50 ohms
```

Example with unequal vaules of resistors:

```
--------+-------------+-------------+
        |             |             |
   +----+----+   +----+----+   +----+----+
   |         |   |         |   |         |
   |  330 Ω  |   |  470 Ω  |   |  820 Ω  |
   |         |   |         |   |         |
   +----+----+   +----+----+   +----+----+
        |             |             |
        +-------------+-------------+-------- = ~ 157 ohms

1 / ( (1 / 330 ohms) + (1 / 470 ohms) + (1 / 820 ohms) ) = 156.80187399827395 Ω
```

The formula can be expressed in mathematics as:

```math
\frac{1}{R_\mathrm{eq}} = \frac{1}{R_1} + \frac{1}{R_2} + \cdots +  \frac{1}{R_n}.
```

...but can be better understood as Ruby code:

```ruby
resistor_one   = 330.0
resistor_two   = 470.0
resistor_three = 820.0

total_ohms = 1 / (
  (1 / resistor_one) + (1 / resistor_two) + (1 / resistor_three)
).round(2)
# => 156.80
```

# Extra Stuff

Written June 2019 by Matthew Nielsen (xunker@pyxidis.org) for the [Utah
Ruby Users Group](https://utruby.org).

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
